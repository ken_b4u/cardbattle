<div data-role="page" id="quest-block-list-page" data-theme="j">
	<header>
		<h1>探索 &gt; 地域一覧 &gt; エリア一覧 &gt; 地区一覧</h1>
	</header>
	<div class="content">
	<ol class="box-list-holder">
		<li class="block-7">
			<a href="" data-href-id="#quest-scene-page" data-param="block=7">
				<div class="block-thumb"></div>
				<div class="block-status">
				<div class="block-name">地区7</div>
				<div class="block-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="block-6">
			<a href="" data-href-id="#quest-scene-page" data-param="block=6">
				<div class="block-thumb"></div>
				<div class="block-status">
				<div class="block-name">地区6</div>
				<div class="block-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="block-5">
			<a href="" data-href-id="#quest-scene-page" data-param="block=5">
				<div class="block-thumb"></div>
				<div class="block-status">
				<div class="block-name">地区5</div>
				<div class="block-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="block-4">
			<a href="" data-href-id="#quest-scene-page" data-param="block=4">
				<div class="block-thumb"></div>
				<div class="block-status">
				<div class="block-name">地区4</div>
				<div class="block-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="block-3">
			<a href="" data-href-id="#quest-scene-page" data-param="block=3">
				<div class="block-thumb"></div>
				<div class="block-status">
				<div class="block-name">地区3</div>
				<div class="block-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="block-2">
			<a href="" data-href-id="#quest-scene-page" data-param="block=2">
				<div class="block-thumb"></div>
				<div class="block-status">
				<div class="block-name">地区2</div>
				<div class="block-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="block-1">
			<a href="" data-href-id="#quest-scene-page" data-param="block=1">
				<div class="block-thumb"></div>
				<div class="block-status">
				<div class="block-name">地区1</div>
				<div class="block-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
	</ol>
	</div>
	<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>
</div>