<div data-role="page" id="gacha-list-page" data-theme="j">
	<header>
		<h1>ガチャ一覧</h1>
	</header>
	<div class="content">
		<ul class="gacha-list box-list-holder">
			<li>
				<a href="" data-href-id="#gacha-scene-page" data-param="gacha_id=1" class="text-button">ガチャ1</a>
			</li>
			<li>
				<a href="" data-href-id="#gacha-scene-page" data-param="gacha_id=2" class="text-button">ガチャ2</a>
			</li>
			<li>
				<a href="" data-href-id="#gacha-scene-page" data-param="gacha_id=3" class="text-button">ガチャ3</a>
			</li>
		</ul>
	</div>
	<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>
</div>