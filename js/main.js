"use strict";
var cb = typeof(cb)=="object"?cb:{};
cb.initialize = function(){
	cb.fitFullScreen();
	var deviceType = "";
	if(cb.isIOS){
		deviceType = "ios";
	} else if (cb.isAndroid){
		deviceType = "android";
	}
	$("html").addClass(deviceType);
	$(document).delegate("a[data-href-id]","click",function(){
		var self = $(this);
		var selector= self.data("href-id");
		var props = self.data("param").split("&");
		var imax = props.length;
		var dataObj = {};
		for(var i in props){
			var prop = props[i].split("=");
			dataObj[prop[0]] = prop[1];
		}
		$.mobile.changePage($(selector).data(dataObj));
		return false;
	}).delegate(".ui-page","pageshow",function(){
		var self = $(this);
		self.off("swipeleft").on("swipeleft",function(){
			history.back();
			self.off("swipeleft");
			return false;
		}).one("pagehide",function(){
			self.off("swipeleft");
		});
	}).delegate("#quest-area-list-page","pagebeforeshow",function(){
		var region = $(this).data("region");
		region = region?region:1;
		$("#quest-block-list-page").attr({"data-region":region});
		$(this).attr({"data-region":region}).find("h1").html("探索 &gt; 地域"+region+" &gt; エリア一覧");
	}).delegate("#quest-block-list-page","pagebeforeshow",function(){
		var region = $(this).attr("data-region");
		region = region?region:1;
		var area = $(this).attr("data-area");
		area = area?area:1;
		$("#quest-scene-page").attr({"data-area":area,"data-region":region});
		$(this).attr({"data-area":area}).find("h1").html("探索 &gt; 地域"+region+" &gt; エリア" +area+" &gt; 地区一覧");
	}).delegate("#quest-scene-page","pagebeforeshow",function(){
		var self = this;
		var region = $(this).attr("data-region");
		region = region?region:1;
		var area = $(this).attr("data-area");
		area = area?area:1;
		var block = $(this).attr("data-block");
		block = block?block:1;
		$(this).attr({"data-area":area,"data-region":region,"data-block":block});
		// ステージ全体
		var stage = new cb.CBLayer();
		stage.setSize({width:320,height:300});
		// 背景
		var field = self.field = new cb.CBSprite();
		field.setSize({width:800,height:300});
		field.setPosition({x:0,y:0});
		field.elem.addClass("field");
		stage.addChild(field);
		$(this).find(".screen").append(stage.elem);
		// キャラクター
		var chara = self.chara = new cb.CBSpriteAnimation();
		var settings = {name:"field-character",src:"./img/characters/pipo-charachip001.png",frames:3,size:{width:32,height:32},imageSize:{width:96,height:32},interval:0,duration:1000,iterationCount:"infinite",delay:0};
		chara.setup(settings);
		chara.setPosition({x:48,y:200});
		stage.addChild(chara);
		// 宝箱
		var box = new cb.Effect();
		box.setup("treasure-box");
		field.addChild(box);
		// ボタン
		var btn = $(this).find(".quest-button").removeClass("disabled");
		// 冒険
		var explorer = function(){
			btn.addClass("disabled");
			// フィールド移動開始
			field.elem.addClass("action");
			field.elem.css({webkitAnimationPlayState:"running"})
			box.setPosition({x:560,y:200});
			// 通信して結果取得
			$.get("./gacha-scene.php",function(data){
				var cnt = 0;
				var selector = "#"+field.elem.attr("id");
				cb.doc.delegate(selector,"webkitAnimationIteration",function(){
					cnt ++
					if(cnt==1){
						setTimeout(function(){
							box.ready();
						},100);
					} else if(cnt==2){
						field.elem.css({webkitAnimationPlayState:"paused"});
						box.setPosition({x:160,y:200});
						cb.doc.undelegate(selector,"webkitAnimationIteration");
						setTimeout(function(){
							box.invoke(true).done(function(){
								$("#quest-treasure-popup").popup("open",{x:10,y:30}).one("popupafterclose",function(){
									btn.removeClass("disabled");
								}).one("popupafteropen",function(){
									box.ready(false).revoke();
								});
							});
						},500);
					}
				})
			})
		}
		btn.on("vclick",explorer);
	}).delegate("#quest-scene-page","pagehide",function(){
		// ステージ消す
		$(this).find(".screen").empty();
		// ボタンリセット
		$(this).find(".quest-button").off("vclick");
	}).delegate("#gacha-scene-page","pagebeforeshow",function(){
		var btn = $(this).find(".button-holder").hide();
		var gacha = new Gacha();
		gacha.setup();
		$(this).one("pageshow",function(){
			setTimeout(function(){
				gacha.play().done(function(){
					btn.show();
				});
			},1000);
		}).find(".screen").append(gacha.elem);
	}).delegate("#gacha-scene-page","pagehide",function(){
		// ステージ消す
		$(this).find(".screen").empty();
		// ボタンリセット
		$(this).find(".gacha-button").off("vclick");
	}).delegate("#battle-scene-page","pagebeforeshow",function(){
		var btn = $(this).find(".button-holder").hide();
		var battle = new Battle();
		battle.setup(window.battlePage.settings);
		$(this).one("pageshow",function(){
			setTimeout(function(){
				battle.play().done(function(){
					btn.show();
				});
			},1000);
		}).find(".screen").append(battle.elem);
	}).delegate("#battle-scene-page","pagehide",function(){
		// ステージ消す
		$(this).find(".screen").empty();
		// ボタンリセット
		$(this).find(".gacha-button").off("vclick");
	});;
}
cb.fitFullScreen = function(){
	if(!cb.isAndroid) return;
	var content = "";
	if(navigator.userAgent.search(/Android 2./) != -1){
		var fixed_width = 320;
		var ratio = window.devicePixelRatio;
		// 現在のdensitydpiを取得する。要はデフォルト値
		var c_dpi = (window.innerWidth * ratio * 160) / window.outerWidth;
		var nd = fixed_width / window.innerWidth * c_dpi;
		// target-densitydpiは70から400までの値が取れる
		if(nd < 70) {
			nd = 70;
		} else if(nd > 400) {
			nd = 400;
		}
		// 小数点第二位に丸める
		var dpi = Math.floor(nd * 100) / 100;
		content = "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,target-densitydpi="+dpi;
	} else {
		var scale = window.outerWidth/320;
		scale = Math.floor(scale * 100)/100;
		content = "width=320,initial-scale="+scale+",minimum-scale="+scale+",maximum-scale="+scale+",user-scalable=yes";
	}
	$("#viewport").attr("content",content);
}