<div data-role="page" id="quest-region-list-page" data-theme="j">
	<header>
		<h1>探索 &gt; 地域一覧</h1>
	</header>
	<div class="content">
	<ol class="box-list-holder">
		<li class="region-7">
			<a href="" data-href-id="#quest-area-list-page" data-param="region=7">
				<div class="region-thumb"></div>
				<div class="region-status">
				<div class="region-name">地域7</div>
				<div class="region-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="region-6">
			<a href="" data-href-id="#quest-area-list-page" data-param="region=6">
				<div class="region-thumb"></div>
				<div class="region-status">
				<div class="region-name">地域6</div>
				<div class="region-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="region-5">
			<a href="" data-href-id="#quest-area-list-page" data-param="region=5">
				<div class="region-thumb"></div>
				<div class="region-status">
				<div class="region-name">地域5</div>
				<div class="region-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="region-4">
			<a href="" data-href-id="#quest-area-list-page" data-param="region=4">
				<div class="region-thumb"></div>
				<div class="region-status">
				<div class="region-name">地域4</div>
				<div class="region-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="region-3">
			<a href="" data-href-id="#quest-area-list-page" data-param="region=3">
				<div class="region-thumb"></div>
				<div class="region-status">
				<div class="region-name">地域3</div>
				<div class="region-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="region-2">
			<a href="" data-href-id="#quest-area-list-page" data-param="region=2">
				<div class="region-thumb"></div>
				<div class="region-status">
				<div class="region-name">地域2</div>
				<div class="region-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="region-1">
			<a href="" data-href-id="#quest-area-list-page" data-param="region=1">
				<div class="region-thumb"></div>
				<div class="region-status">
				<div class="region-name">地域1</div>
				<div class="region-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
	</ol>
	</div>
	<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>
</div>